﻿Shader "Sprites/LineRender"
{
	Properties
	{
		 _MainTex("Sprite Texture", 2D) = "white" {}
		[HDR]_Color("Tint", Color) = (1,1,1,1)
		_ColorCharacter("ColorCharacter", Color) = (1,1,1,1)
		_BumpMap("Normalmap", 2D) = "bump" {}
		_BumpIntensity("NormalMap Intensity", Range(-1, 2)) = 1
		_BumpIntensity("NormalMap Intensity", Float) = 1
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		_Cutoff("Alpha Cutoff", Range(0,1)) = 0.5


		_Distort("Distort Texture", 2D) = "grey" {}
		_Offset("Offset", Range(-4,10)) = 0
		_Multiplier("Sprite Multiplier", Range(-2,2)) = 1
		_Scale("Distort Scale", Range(0,10)) = 3.2
		_SpeedX("Speed X", Range(-10,10)) = 2
		_SpeedY("Speed Y", Range(-10,10)) = -3.2
		_EffectSize("Effect Size", Range(-0.1,0.1)) = -0.03
		_EffectOffset("Effect Offset", Range(-0.1,0.1)) = 0
		_Brightness("Brightness", Range(-10,10)) = 1.5
		_Opacity("Opacity", Range(-5,10)) = 1.2
			_IsClignote("IsClignote", Float) = 0
		[Toggle(ORDER)] _ORDER("Aura Behind", Float) = 1
		[Toggle(ONLY)] _ONLY("Only Aura", Float) = 0
		[Toggle(GRADIENT)] _GRADIENT("Fade To Bottom", Float) = 1
	}
		SubShader
		{
			Tags
			{
				"Queue" = "AlphaTest"
				"IgnoreProjector" = "True"
				"RenderType" = "TransparentCutOut"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}
			LOD 300
			Cull Off
			Lighting On
			ZWrite On
					ZTest Off

			Fog { Mode Off }
			CGPROGRAM
			#pragma target 3.0
			#pragma surface surf Standard alpha vertex:vert  alphatest:_Cutoff fullforwardshadows
			#pragma multi_compile DUMMY PIXELSNAP_ON 
			#pragma exclude_renderers flash
				#pragma shader_feature ORDER               
				#pragma shader_feature GRADIENT            
				#pragma shader_feature ONLY

			sampler2D _MainTex, _Distort;
			sampler2D _BumpMap;
			fixed _BumpIntensity;

			float4 _Color, _ColorCharacter;
			float _Offset;
			float _Brightness;
			float _EffectSize;
			float _EffectOffset;
			float _Scale;
			float _SpeedY, _SpeedX;
			float _Opacity, _IsClignote;
			float _Multiplier;
			struct Input
			{
				float2 uv_MainTex;
				float2 uv_BumpMap;
				fixed4 color;
			};

			void vert(inout appdata_full v, out Input o)
			{
				#if defined(PIXELSNAP_ON) && !defined(SHADER_API_FLASH)
				v.vertex = UnityPixelSnap(v.vertex);
				#endif
				//v.normal = float3(0,0,-1);
				//v.tangent = float4(1, 0, 0, 1);
				UNITY_INITIALIZE_OUTPUT(Input, o);
				o.color = _Color;
			} 
			void surf(Input i, inout SurfaceOutputStandard o)
			{
				fixed4 c = tex2D(_MainTex, i.uv_MainTex);
				c *= c.a;
				// scale UV
				float2 scaledUV = (i.uv_MainTex * (1 + _EffectSize)) - _EffectOffset;
				// UV movement
				float timeX = _Time.x * _SpeedX;
				float timeY = _Time.x * _SpeedY;

				//move the distort textures uv and scale them
				float d = tex2D(_Distort, float2(i.uv_MainTex.x * _Scale + timeX, i.uv_MainTex.y * _Scale + timeY)) * 0.1;
				// set the texture over distorted uvs
				float4 r = tex2D(_MainTex, float2(scaledUV.x + d, scaledUV.y + d));
				// multiply by alpha for cutoff
				r *= r.a;
#if GRADIENT
				// gradient over UV vertically
				r *= lerp(0, 1, (i.uv_MainTex.y + _Offset));
#endif
#if ORDER
				// delete original sprite's alpha to only have the outline
				r *= (1 - c.a);
#endif
				// extra color tinting with multiplier to original sprite
				float4 tinting = _Color + (c * _Multiplier);
				// add brightness
				r += (_Brightness * r.a) * tinting;
				// set effect opacity
				if (_IsClignote == 1)
				{
					_Opacity *= abs(sin(_Time.z*1.65));
				}
				r = saturate(r *_Opacity);

				fixed4 Final = r + c;

#if ONLY
				 Final = r;
#endif 

				o.Albedo = saturate(c.rgb*_ColorCharacter); 
				o.Emission = r.rgb*5;
				o.Alpha = Final.a;
				o.Metallic = 1;
				o.Smoothness = 0;
				o.Normal = UnpackNormal(tex2D(_BumpMap, i.uv_BumpMap));
				_BumpIntensity = 1 / _BumpIntensity;
				o.Normal.z = o.Normal.z * _BumpIntensity;
				o.Normal = normalize((half3)o.Normal);
			}
			ENDCG
		}
			Fallback "Transparent/Cutout/Diffuse"
}