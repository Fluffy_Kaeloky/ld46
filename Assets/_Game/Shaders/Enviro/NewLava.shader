Shader "Unlit/SimpleLava"
{
	Properties
	{
		[Header(Main)]
		_Color("Main Tint Start", Color) = (0.5, 0.1,0.1, 1)
		_Color2("Main Tint End", Color) = (0.6, 0.4,0.1, 1)
		_Offset("Start/End Tint Offset", Range(0,10)) = 1
		_MainTex("Main Texture", 2D) = "white" {}
		_Color3("Top Layer Tint", Color) = (1, 1,0, 1)
		_Scale("Scale Main", Range(0,5)) = 0.3
		_SpeedMainX("Speed Main X", Range(-10,10)) = 0.4
		_SpeedMainY("Speed Main Y", Range(-10,10)) = 0.4
		_Strength("Brightness Under Lava", Range(0,10)) = 2
		_StrengthTop("Brightness Top Lava", Range(0,10)) = 3
		_Cutoff("Cutoff Top", Range(0,1)) = 0.9
		_TopBlur("Top Blur", Range(0,1)) = 0.1

		[Space(10)]
		[Header(Edge)]
		_EdgeC("Edge Color", Color) = (1, 0.5, 0.2, 1)
		[HDR]_EdgeD("Depth Color", Color) = (1, 0.5, 0.2, 1)
		_EdgeBlur("Edge Blur", Range(0,1)) = 0.5
		_Edge("Edge Thickness", Range(0,20)) = 8

		[Space(10)]
		[Header(Distortion)]
		_DistortTex("Distort Texture", 2D) = "white" {}
		_ScaleDist("Scale Distortion", Range(0,10)) = 0.5
		_SpeedDistortX("Speed Distort X", Range(-10,10)) = 0.2
		_SpeedDistortY("Speed Distort Y", Range(-10,10)) = 0.2
		_Distortion("Distort Strength", Range(0,1)) = 0.2
		_VertexDistortion("Extra Vertex Color Distortion", Range(0,1)) = 0.3

		[Space(10)]
		[Header(Vertex Movement)]
		_Speed("Wave Speed", Range(0,1)) = 0.5
		_Amount("Wave Amount", Range(0,100)) = 0.6
		_Height("Wave Height", Range(0,1)) = 0.1
		_FogStart("Fog Start", Float) = 0.0
		_FogEnd("Fog End", Float) = 10.0
			_FogStart2("Fog Start2", Float) = 0.0
		_FogEnd2("Fog End2", Float) = 10.0
					_FogColor("Fog Color (RGB)", Color) = (0.5, 0.5, 0.5, 1.0)
					_RippleValue("RippleValue", Range(0,10)) = 5

	}
		SubShader
		{
			Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent" }
			LOD 100

			Pass
		{
			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
			// make fog work
	#pragma multi_compile_fog
	#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float4 color: COLOR;
			
		};

		struct v2f
		{
			float2 uv : TEXCOORD3;
			UNITY_FOG_COORDS(1)
			float4 vertex : SV_POSITION;
			float4 scrPos : TEXCOORD2;//
			float4 worldPos : TEXCOORD4;//
			float4 color :COLOR;

			float fogVar : TEXCOORD5;
			float fogVar2 : TEXCOORD6;
		};

		uniform sampler2D _GlobalEffectRTLAVA;

		uniform float _OrthographicCamSizeLAVA;

		sampler2D _CameraDepthTexture;
		uniform float3 _Position;

		float4 _Color, _Color2, _Color3, _EdgeC, _EdgeD;
		sampler2D _MainTex, _DistortTex;
		float4 _MainTex_ST;
		float _Speed, _Amount, _Height, _Edge, _Scale, _ScaleDist;
		float  _Offset, _Strength, _Distortion, _Cutoff, _StrengthTop;
		float _EdgeBlur, _VertexDistortion;
		float _SpeedDistortX, _SpeedDistortY;
		float _SpeedMainX, _SpeedMainY;
		float _TopBlur, _RippleValue;
		float _FogStart;
		float _FogEnd;
		float _FogStart2;
		float _FogEnd2;
		fixed4 _FogColor;

		v2f vert(appdata v)
		{
			v2f o;
			UNITY_INITIALIZE_OUTPUT(v2f, o);

			//wave movement multiplied by vertex color

			o.vertex = UnityObjectToClipPos(v.vertex);

			// world position for textures
			//o.worldPos = mul(unity_ObjectToWorld, v.vertex);

			// vertex colors
			o.color = 1-v.color;

			// screen position for depth


			float zpos = UnityObjectToClipPos(v.vertex).y;

			o.vertex = mul(UNITY_MATRIX_MV, o.vertex);

			o.fogVar = saturate(1.0 - (_FogEnd*1000 + o.vertex.z) / (_FogEnd * 1000 - _FogStart * 1000));
			o.fogVar2 = saturate(1.0 - (_FogEnd2 * 1000 + o.vertex.z) / (_FogEnd2 * 1000 - (_FogStart2 * 1000)));
			o.worldPos = mul(unity_ObjectToWorld, v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);

			float2 uvLAVA = o.worldPos.xz - _Position.xz;
			uvLAVA = uvLAVA / (_OrthographicCamSizeLAVA * 2);
			uvLAVA += 0.5;
			float ripples = saturate(1 - tex2Dlod(_GlobalEffectRTLAVA, float4(uvLAVA, 0, 0)).r);
			float ripplesGreen = saturate(1 - tex2Dlod(_GlobalEffectRTLAVA, float4(uvLAVA, 0, 0)).g);
			v.vertex.y = v.vertex.y - (1 - ripples) * 0.05;
			v.vertex.y = v.vertex.y + (1 - ripplesGreen) * 0.2;
			v.vertex.y += ((sin(_Time.z * _Speed + (v.vertex.x * v.vertex.z * _Amount*ripples)) * _Height))*ripples;
			v.vertex.x += ((sin(_Time.z * _Speed + (v.vertex.x * v.vertex.z * _Amount*ripples)) * _Height))*ripples;
			v.vertex.z += ((sin(_Time.z * _Speed + (v.vertex.x * v.vertex.z * _Amount*ripples)) * _Height))*ripples;

			o.vertex = UnityObjectToClipPos(v.vertex);


			o.scrPos = ComputeScreenPos(o.vertex);

			UNITY_TRANSFER_FOG(o,o.vertex);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			fixed3 fogColor = _FogColor.rgb;

			float2 uvLAVA = i.worldPos.xz - _Position.xz;
			uvLAVA = uvLAVA / (_OrthographicCamSizeLAVA * 2);
			uvLAVA += 0.5;
			// uv distortion scaled
			float2 uvDistort = i.uv * _ScaleDist;
			// moving over time
			float speedDistortX = _Time.x * _SpeedDistortX;
			float speedDistortY = _Time.x * _SpeedDistortY;
			float2 speedDistortCombined = float2(speedDistortX, speedDistortY);

			// distortion textures at different scales
			float d = tex2D(_DistortTex, uvDistort + speedDistortCombined).r;
			float d2 = tex2D(_DistortTex, (i.uv * (_ScaleDist * 0.5)) + speedDistortCombined).r;
			// combined
			float layereddist = saturate((d + d2)*0.5);

			// uv main scaled
			float2 uvMain = i.uv * _Scale;
			// plus distortion
			uvMain += layereddist * _Distortion;

			// moving over time
			float speedMainX = _Time.x * _SpeedMainX;
			float speedMainY = _Time.x * _SpeedMainY;
			float2 speedMainCombined = float2(speedMainX, speedMainY);
			//float2 speedMainCombined = lerp(float2(0, 0), float2(speedMainX, speedMainY),smoothstep( 0.89,0.92+sin(_Time.x * _SpeedMainY), RippleColor));

			// depth edge detection
			half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)));
			half4 edgeLine = 1 - saturate(_Edge* (depth - i.scrPos.w));

			//half4 RippleColor = i.color.r*pow(ripples, 5);

			// main uv moving with extra distortion based on vertex colors
			uvMain += speedMainCombined + ( _VertexDistortion);
			uvMain += speedMainCombined + (edgeLine * _VertexDistortion*0.3*edgeLine);

			// main texture fading with vertex color
			half4 col = tex2D(_MainTex, uvMain);
			//col = saturate( pow(col* RippleColor,5));


			
			
			col += layereddist;





			// top layer
			//float top = smoothstep(_Cutoff, _Cutoff + _TopBlur, col) * RippleColor;
			float top = 0;



			// cutoff edge based on main texture
			float edge = smoothstep(1 - col , 1 - col + _EdgeBlur, edgeLine);

			// lerp start and end color over main texture, multiply for brightness
			float4 color = lerp(_Color, _Color2, col * _Offset) * _Strength;
			// take edge out of main color
			//color *= (1 - edge);

			// take the top out of main color
			//color *= (1 - top);

			// fade using the vertex color
			//color *= i.color.r; // WHAT YOU DO BOIIIIIiiiii
			// add edge back in colored, multiply for brightness
			//color += (edge *_EdgeC) * _StrengthTop;
			color += (edge *_EdgeC) * _StrengthTop;
			//color += _EdgeD * edgeLine*RippleColor.r;
			//color += _EdgeD * (1 - saturate(8* (depth - i.scrPos.w)));
			// add top back in colored, multiply for brightness
			//color += top * _Color3 *_StrengthTop;
			//color.rgb = lerp(color.rgb, fogColor * 2, i.fogVar*_FogColor.a);
			//color.rgb = lerp(color.rgb, fogColor * 2, i.fogVar*_FogColor.a);
			//color.rgb = lerp(color.rgb, unity_FogColor, i.fogVar2);
			float4 colorFog = 0;
			UNITY_APPLY_FOG(i.fogCoord*(1, 1, 3, 1 * _FogEnd)+(0,0,0,-0.2), colorFog);

			colorFog = (colorFog.r* (1 / unity_FogColor.r) + colorFog.g* (1 / unity_FogColor.g) + colorFog.b* (1 / unity_FogColor.b)) / 3;
			//color.rgb = color.rgb+(colorFog.rgb*_FogColor.rgb);
			color.rgb = lerp( color.rgb,(colorFog.rgb*_FogColor.rgb), colorFog);
			UNITY_APPLY_FOG(i.fogCoord*(3,3,3,3 * _FogEnd), color);

			//color.rgb = color.rgb * pow(ripples,5);
			//color.rgb = colorFog * fogColor.rgb;

			return color;
			}
			ENDCG
		}
		}
}