﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaveManager : MonoBehaviour
{
    public static WaveManager Instance { get; private set; } = null;

    public List<LevelDescriptor> descriptors = new List<LevelDescriptor>();

    public UnityEvent onGameEnded = new UnityEvent();
    public UnityEvent onTransitionPlay = new UnityEvent();

    public Animator transitionAnimator = null;
    public string transitionAnimatorParameterName = "Play";

    private EnemySpawner enemySpawner = null;

    private int currentDescriptorIndex = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        enemySpawner = EnemySpawner.Instance;

        NextLevel();
    }

    public void NextLevel()
    {
        if (currentDescriptorIndex >= descriptors.Count)
        {
            onGameEnded.Invoke();
            GameManager.Instance.GameEnd();
            return;
        }

        foreach (EnemySpawnDescriptor descriptor in descriptors[currentDescriptorIndex].enemyDescriptors)
            enemySpawner.SpawnEnemy(descriptor.entity, descriptor.column, descriptor.line);

        EnvironnementManager.Instance.EnableEnvironement(descriptors[currentDescriptorIndex].environementIndex);

        GameBattleManager.Instance.StartBattle();

        ++currentDescriptorIndex;
    }

    public void PlayTransition()
    {
        transitionAnimator.SetTrigger(transitionAnimatorParameterName);
        onTransitionPlay.Invoke();
    }

    public DiceRewardDescriptor GetCurrentLevelDiceRewards()
    {
        return descriptors[currentDescriptorIndex - 1].diceRewards;
    }
}
