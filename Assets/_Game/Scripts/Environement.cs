﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Environement : MonoBehaviour
{
    public Transform battleManagerHandle = null;

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);

        if (active)
        {
            GameBattleManager.Instance.transform.position = battleManagerHandle.position;
            GameBattleManager.Instance.transform.rotation = battleManagerHandle.rotation;
            GameBattleManager.Instance.transform.localScale = battleManagerHandle.localScale;
        }
    }
}
