﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DiceHand : MonoBehaviour
{
    #region Classes declarations

    [Serializable]
    public class OnGeneratorLockdown : UnityEvent<OnGeneratorLockdownArgs> { }

    [Serializable]
    public class OnGeneratorLockdownArgs
    {
        public DiceGenerator DiceGenerator { get; private set; } = null;

        public OnGeneratorLockdownArgs(DiceGenerator diceGenerator)
        {
            DiceGenerator = diceGenerator;
        }
    }
    
    #endregion

    public List<DiceGenerator> diceGenerators = new List<DiceGenerator>();

    public Transform diceHandle = null;

    public float diceHandScale = 0.5f;
    public float diceSpacing = 5.0f;

    public float tweenDuration = 0.5f;

    public GeneratorLockdownUI lockdownUIPrefab = null;

    public OnGeneratorLockdown onGeneratorLockdown = new OnGeneratorLockdown();

    private async void Start()
    {
        diceGenerators.ForEach(x => RegisterCallbacks(x));

        await new WaitForEndOfFrame();

        UpdateLayout();
    }

    public void AddDiceGenerator(DiceGenerator dice)
    {
        diceGenerators.Add(dice);
        RegisterCallbacks(dice);

        dice.transform.SetParent(diceHandle, true);
        dice.transform.localRotation = Quaternion.identity;

        UpdateLayout();
    }

    public void RemoveDiceGenerator(DiceGenerator dice)
    {
        diceGenerators.Remove(dice);
        RemoveCallbacks(dice);
        UpdateLayout();
    }

    public void UpdateLayout()
    {
        for (int i = 0; i < diceGenerators.Count; i++)
        {
            diceGenerators[i].transform.DOMove(diceHandle.transform.position + diceHandle.transform.right * i * diceSpacing, tweenDuration)
                .SetEase(Ease.OutExpo);
            diceGenerators[i].transform.localScale = Vector3.one * diceHandScale;
        }
    }

    public void HideUI()
    {
        Debug.LogError("Hide UI");

        RectTransform rect = (RectTransform)transform;

        rect.DOAnchorPosY(rect.rect.y / 3.0f, 1.5f)
            .SetEase(Ease.OutElastic);
    }

    private void RegisterCallbacks(DiceGenerator diceGenerator)
    {
        diceGenerator.onDiceGenerated.AddListener(OnDiceGenerated);
    }

    private void RemoveCallbacks(DiceGenerator diceGenerator)
    {
        diceGenerator.onDiceGenerated.RemoveListener(OnDiceGenerated);
    }

    private void OnDiceGenerated(DiceGenerator.OnDiceGeneratedArgs args)
    {
        StartCoroutine(GeneratorLockdown(args.Sender));
    }

    private IEnumerator GeneratorLockdown(DiceGenerator generator)
    {
        generator.CanGenerateDice = false;

        onGeneratorLockdown.Invoke(new OnGeneratorLockdownArgs(generator));

        GeneratorLockdownUI lockdownUI = Instantiate(lockdownUIPrefab, generator.transform);

        lockdownUI.Init(generator.cooldown);

        yield return new WaitForSeconds(generator.cooldown);

        generator.CanGenerateDice = true;
    }
}
