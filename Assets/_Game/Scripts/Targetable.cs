﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targetable : MonoBehaviour
{

    public void ForceTarget(SelectableObject.OnClickArgs args)
    {
        DemonManager.Instance.ForcePossessedFocus(args);
    }

}
