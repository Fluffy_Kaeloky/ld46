﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public abstract class BattleController : MonoBehaviour
{
    public float chargeRateMultiplier = 1.0f;
    public float NextAttackMultiplier = 1.0f;

    public abstract void InitBattle(List<BattleEntity> entities, List<BattleEntity> enemyEntities);
    public abstract void UpdateController(List<BattleEntity> entities, List<BattleEntity> enemyEntities);
    public abstract BattleEntity RequestTarget();
}
