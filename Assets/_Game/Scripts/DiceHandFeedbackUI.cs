﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceHandFeedbackUI : MonoBehaviour
{
    public Image HoverSprite;

    private void Start()
    {
        HoverSprite.enabled = false;
    }

    public void HoverActivate()
    {
        HoverSprite.enabled = true;
    }
    
    public void HoverDeactivate()
    {
        HoverSprite.enabled = false;
    }




}
