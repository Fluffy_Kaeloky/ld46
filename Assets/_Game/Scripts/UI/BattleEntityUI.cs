﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleEntityUI : MonoBehaviour
{
    public BattleEntity entity = null;

    public Slider healthSlider = null;
    public Slider actionSlider = null;

    public void Start()
    {
        entity.onDamageTaken.AddListener((args) => { UpdateUI(); } );
        entity.onDeath.AddListener(UpdateUI);
        entity.onActionChargeChanged.AddListener(UpdateUI);
        entity.onHeal.AddListener((args) => UpdateUI());
    }

    private void UpdateUI()
    {
        healthSlider.value = entity.lifeAmount / entity.maxLifeAmount;
        actionSlider.value = entity.actionCharge;
    }
}
