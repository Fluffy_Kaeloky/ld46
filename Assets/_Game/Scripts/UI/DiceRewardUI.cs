﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using UnityEngine.Events;

public class DiceRewardUI : MonoBehaviour
{
    public CanvasGroup canvasGroup = null;

    public DiceHand diceHand = null;

    public Transform leftDiceHandle = null;
    public Transform rightDiceHandle = null;

    private DiceGenerator leftPrefab = null;
    private DiceGenerator rightPrefab = null;

    public UnityEvent onRewardChosen = new UnityEvent();

    public bool IsOpen { get; private set; } = false;
    public float SizeReward = 1.3f;

    private void Start()
    {
        canvasGroup.alpha = 0.0f;
        gameObject.SetActive(false);
        IsOpen = false;

        leftDiceHandle.localScale = Vector3.zero;
        rightDiceHandle.localScale = Vector3.zero;
    }

    public void Initialize(DiceGenerator prefabLeft, DiceGenerator prefabRight)
    {
        DestroyDicePreviews();

        leftPrefab = prefabLeft;
        rightPrefab = prefabRight;

        DiceGenerator leftInstance = Instantiate(prefabLeft, leftDiceHandle, false);
        leftInstance.transform.localPosition = Vector3.zero;
        leftInstance.CanGenerateDice = false;
        leftInstance.GetComponentsInChildren<Collider>().ToList().ForEach(x => x.enabled = false);
        leftInstance.transform.localScale = Vector3.one * SizeReward;

        DiceGenerator rightInstance = Instantiate(prefabRight, rightDiceHandle, false);
        rightInstance.transform.localPosition = Vector3.zero;
        rightInstance.CanGenerateDice = false;
        rightInstance.GetComponentsInChildren<Collider>().ToList().ForEach(x => x.enabled = false);
        rightInstance.transform.localScale = Vector3.one * SizeReward;
    }

    public void OpenScreen()
    {
        gameObject.SetActive(true);
        IsOpen = true;

        canvasGroup.alpha = 0.0f;
        canvasGroup.DOFade(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);

        leftDiceHandle.DOScale(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);
        rightDiceHandle.DOScale(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);
    }

    public void CloseScreen()
    {
        IsOpen = false;

        canvasGroup.DOFade(0.0f, 0.5f)
            .SetEase(Ease.OutExpo)
            .OnComplete(() => 
            {
                gameObject.SetActive(false);
            });

        leftDiceHandle.DOScale(0.0f, 0.5f)
            .SetEase(Ease.OutExpo);
        rightDiceHandle.DOScale(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);
    }

    public void ChooseRewardLeft()
    {
        if (!IsOpen)
            return;

        diceHand.AddDiceGenerator(Instantiate(leftPrefab));
        CloseScreen();
        onRewardChosen.Invoke();
    }

    public void ChooseRewardRight()
    {
        if (!IsOpen)
            return;

        diceHand.AddDiceGenerator(Instantiate(rightPrefab));
        CloseScreen();
        onRewardChosen.Invoke();
    }

    private void DestroyDicePreviews()
    {
        if (leftDiceHandle.childCount > 0)
            Destroy(leftDiceHandle.GetChild(0).gameObject);
        if (rightDiceHandle.childCount > 0)
            Destroy(rightDiceHandle.GetChild(0).gameObject);
    }
}
