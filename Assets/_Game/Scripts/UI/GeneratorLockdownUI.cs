﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class GeneratorLockdownUI : MonoBehaviour
{
    public float startOffset = 5.0f;

    public Image darkForeground = null;

    private CanvasGroup canvasGroup = null;

    private float startTimer = 0.0f;
    private float timer = 0.0f;

    public void Init(float duration)
    {
        startTimer = duration;
        timer = duration;
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        canvasGroup.alpha = 0.0f;
        canvasGroup.DOFade(1.0f, 1.0f)
            .SetEase(Ease.OutElastic);
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        UpdateUI();

        if (timer <= 0.0f)
            Destroy(gameObject);
    }

    private void UpdateUI()
    {
        if (darkForeground != null)
            darkForeground.fillAmount = timer / startTimer;
    }
}
