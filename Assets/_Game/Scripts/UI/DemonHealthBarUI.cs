﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DemonHealthBarUI : MonoBehaviour
{
    public DemonManager demonManager = null;

    public Slider slider = null;

    private void Start()
    {
        demonManager.onLifeChanged.AddListener(UpdateUI);
        slider.value = demonManager.life / demonManager.maxLife;
    }

    private void UpdateUI()
    {
        slider.DOValue(demonManager.life / demonManager.maxLife, 0.5f)
            .SetEase(Ease.OutExpo);
    }
}
