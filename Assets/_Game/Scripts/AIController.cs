﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : BattleController
{
    public override void InitBattle(List<BattleEntity> entities, List<BattleEntity> enemyEntities)
    {
        if (enemyEntities.Count > 0)
            entities.ForEach(x => {
                x.SetTarget(enemyEntities[Random.Range(0, enemyEntities.Count)]);
                x.onDeath.AddListener(() => 
                {
                    GameBattleManager.Instance.RemoveEntity(x);
                });
            });
    }

    public override BattleEntity RequestTarget()
    {
        GameBattleManager battleManager = GameBattleManager.Instance;

        if (this == battleManager.rightSideController)
        {
            if (battleManager.leftSideEntities.Count > 0)
                return battleManager.leftSideEntities[Random.Range(0, battleManager.leftSideEntities.Count)];
        }
        else if (this == battleManager.leftSideController)
        {
            if (battleManager.rightSideEntities.Count > 0)
                return battleManager.rightSideEntities[Random.Range(0, battleManager.rightSideEntities.Count)];
        }

        return null;
    }

    public override void UpdateController(List<BattleEntity> entities, List<BattleEntity> enemyEntities)
    {
        foreach (BattleEntity entity in entities)
        {
            if (entity.IsReady)
            {
                entity.Attack(NextAttackMultiplier);
                NextAttackMultiplier = 1.0f;

                if (enemyEntities.Count > 0)
                {
                    if (entity.Target == null || (entity.Target != null && entity.Target.IsDead))
                        entity.SetTarget(enemyEntities[Random.Range(0, enemyEntities.Count)]);
                }

                entity.actionCharge = 0.0f;
            }
        }
    }
}
