﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDamageParticle : MonoBehaviour
{
    public float MoveUp;
    private TextMesh TheText;
    private float FinalSpeed;
    private Color StartColor;
    private float t;
    private Vector3 ScaleStart;
    public float Speed;
    private Vector3 PosStart;
    // Start is called before the first frame update
    void Start()
    {
        PosStart = this.transform.position;
        TheText = this.GetComponent<TextMesh>();
        StartColor = TheText.color;
        ScaleStart = this.transform.localScale;
        ScaleStart = ScaleStart * Random.Range(0.8f, 1.2f);

        PosStart = new Vector3(PosStart.x + Random.Range(-1.2f, 1.2f), PosStart.y + Random.Range(-1.2f, 1.2f), PosStart.z + Random.Range(-1.2f, 1.2f));
        this.transform.position = PosStart;
    }

    // Update is called once per frame
    void Update()
    {
        FinalSpeed = Time.deltaTime/ Speed;
        t += FinalSpeed;
        this.transform.Translate(new Vector3(0, MoveUp * FinalSpeed, 0));
        TheText.color = new Color(StartColor.r, StartColor.g, StartColor.b, Mathf.Lerp(1, 0, t*t));
        this.transform.localScale = Vector3.Lerp(ScaleStart, ScaleStart / 3, t);
        if(t >=1)
        {
            Destroy(this.gameObject);
        }
    }
}
