﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleEntity : MonoBehaviour
{
    #region Classes declarations

    [Serializable]
    public class OnEntityAttack : UnityEvent<OnEntityAttackArgs> { }

    [Serializable]
    public class OnEntityAttackArgs
    {
        public BattleEntity Target { get; private set; } = null;
        public float Damages { get; private set; } = 0.0f;

        public OnEntityAttackArgs(BattleEntity target, float damages)
        {
            Target = target;
            Damages = damages;
        }
    }

    [Serializable]
    public class OnTargetChanged : UnityEvent<OnTargetChangedArgs> { }

    [Serializable]
    public class OnTargetChangedArgs
    {
        public BattleEntity Sender { get; private set; } = null;

        public BattleEntity OldTarget { get; private set; } = null;
        public BattleEntity NewTarget { get; private set; } = null;

        public OnTargetChangedArgs(BattleEntity sender, BattleEntity oldTarget, BattleEntity newTarget)
        {
            Sender = sender;
            OldTarget = oldTarget;
            NewTarget = newTarget;
        }
    }

    [Serializable]
    public class OnDamageTaken : UnityEvent<OnDamageTakenArgs> { }

    [Serializable]
    public class OnDamageTakenArgs
    {
        public BattleEntity Sender { get; private set; } = null;
        public BattleEntity Instigator { get; private set; } = null;
        public float Damages { get; private set; } = 0.0f;

        public OnDamageTakenArgs(BattleEntity sender, BattleEntity instigator, float damages)
        {
            Sender = sender;
            Instigator = instigator;
            Damages = damages;
        }
    }

    [Serializable]
    public class OnHeal : UnityEvent<OnHealArgs> { }

    [Serializable]
    public class OnHealArgs
    {
        public BattleEntity Sender { get; private set; } = null;
        public float Amount { get; private set; } = 0.0f;

        public OnHealArgs(BattleEntity sender, float amount)
        {
            Sender = sender;
            Amount = amount;
        }
    }

    #endregion

    public bool IsReady { get { return actionCharge >= 1.0f && !IsDead; } }
    public bool IsDead { get { return lifeAmount <= 0.0f; } }

    public new string name = "ProtoEnemy";

    [Range(0.0f, 1.0f)]
    public float actionCharge = 0.0f;
    public float actionChargeMultiplier = 0.25f;

    public float minDamage = 1.0f;
    public float maxDamage = 1.0f;

    public float lifeAmount = 10.0f;
    public float maxLifeAmount = 10.0f;

    private bool invulnerable = false;
    public bool Invulnerable
    {
        get { return invulnerable; }
        set
        {
            invulnerable = value;
            onInvulnerabilityStatusChanged.Invoke();
        }
    }

    private bool frozen = false;
    public bool Frozen
    {
        get { return frozen; }
        set
        {
            frozen = value;
            onFrozenStatusChanged.Invoke();
        }
    }

    public BattleEntity Target { get; private set; } = null;

    public OnEntityAttack onEntityAttack = new OnEntityAttack();
    public UnityEvent onDeath = new UnityEvent();
    public OnDamageTaken onDamageTaken = new OnDamageTaken();
    public OnHeal onHeal = new OnHeal();
    public UnityEvent onInvulnerabilityStatusChanged = new UnityEvent();
    public UnityEvent onFrozenStatusChanged = new UnityEvent();
    public UnityEvent onActionChargeChanged = new UnityEvent();

    public OnTargetChanged onTargetChanged = new OnTargetChanged();
    public OnTargetChanged onTargetForced = new OnTargetChanged();

    public void UpdateEntity(BattleController controller)
    {
        if (IsDead)
            return;

        if (!Frozen)
        {
            actionCharge = Mathf.Min(actionCharge + actionChargeMultiplier * controller.chargeRateMultiplier * Time.deltaTime, 1.0f);
            onActionChargeChanged.Invoke();
        }
    }

    public void SetTarget(BattleEntity target)
    {
        BattleEntity oldTarget = Target;
        Target = target;

        onTargetChanged.Invoke(new OnTargetChangedArgs(this, oldTarget, Target));
    }

    public void Attack(float damageMultiplier = 1.0f)
    {
        if (IsDead)
            return;

        if (Target == null)
        {
            if ((Target = GameBattleManager.Instance.GetControllingController(this).RequestTarget()) == null)
                return;
        }

        float damages = Mathf.Round(UnityEngine.Random.Range(minDamage, maxDamage) * damageMultiplier);
        Debug.Log(name + " attacks " + Target.name + " for " + damages);

        onEntityAttack.Invoke(new OnEntityAttackArgs(Target, damages));

        Target.Damage(damages, this);
    }

    public virtual void Damage(float damage, BattleEntity instigator)
    {
        if (Invulnerable)
            return;

        if (lifeAmount <= 0.0f)
            return;

        lifeAmount = Mathf.Max(lifeAmount - damage, 0.0f);

        if (lifeAmount == 0.0f)
        {
            Debug.Log(name + " has died by the hand of " + (instigator != null ? instigator.name : "WORLD"));
            onDeath.Invoke();
            return;
        }

        onDamageTaken.Invoke(new OnDamageTakenArgs(this, instigator, damage));
    }

    public void Heal(float amount)
    {
        if (lifeAmount == 0)
            return;

        lifeAmount = Mathf.Min(lifeAmount + amount, maxLifeAmount);
        onHeal.Invoke(new OnHealArgs(this, amount));
    }

    public void Kill()
    {
        if (lifeAmount <= 0.0f)
            return;

        lifeAmount = 0.0f;
        onDeath.Invoke();
        Debug.Log(name + " was instakilled by the hand of god.");
    }

    public void ForceTarget(BattleEntity entity)
    {
        if (entity == null)
            return;

        BattleEntity oldTarget = Target;
        Target = entity;
        onTargetForced.Invoke(new OnTargetChangedArgs(this, oldTarget, Target));

        Debug.Log(name + " target forced to " + Target.name);
    }
}
