﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroController : MonoBehaviour
{
    public List<string> texts = new List<string>();

    public int stateNumber = 2;

    public Image foreground = null;

    public RectTransform textBoxHandle = null;

    public TMPro.TextMeshProUGUI text = null;
    public Animator animator = null;

    public Image arrow = null;

    private int currentStateNumber = 0;

    private void Start()
    {
        textBoxHandle.localScale = new Vector3(0.0f, 0.05f, 1.0f);
        arrow.enabled = false;

        arrow.DOFade(0.0f, 0.5f)
            .SetEase(Ease.InOutExpo)
            .SetLoops(-1);
    }

    public void OpenTextBox()
    {
        textBoxHandle.DOScaleX(1.0f, 0.5f)
            .SetEase(Ease.Linear)
            .OnComplete(() => 
            {
                textBoxHandle.DOScaleY(1.0f, 0.5f);
            });
    }

    public void CloseTextBox()
    {
        textBoxHandle.DOScaleY(0.05f, 0.5f)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                textBoxHandle.DOScaleX(0.0f, 0.5f);
            });
    }

    public void WriteText(int index)
    {
        text.text = texts[index];
    }

    public void WaitForInput()
    {
        StartCoroutine(_WaitForInput());
    }

    private IEnumerator _WaitForInput()
    {
        Debug.Log("Awaiting input");

        arrow.enabled = true;

        yield return new WaitUntil(() => Input.anyKeyDown);

        arrow.enabled = false;

        ++currentStateNumber;

        if (currentStateNumber >= stateNumber)
        {
            foreground.DOFade(1.0f, 2.0f)
                .OnComplete(() => {
                    Debug.Log("Load level");

                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                });
        }
        else
            animator.SetInteger("IntroState", currentStateNumber);
    }
}
