﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironnementManager : MonoBehaviour
{
    public static EnvironnementManager Instance { get; private set; } = null;

    public List<Environement> environements = new List<Environement>();

    private void Awake() =>
        Instance = this;

    public void EnableEnvironement(int index)
    {
        environements.ForEach(x => x.SetActive(false));

        environements[index].SetActive(true);
    }
}
