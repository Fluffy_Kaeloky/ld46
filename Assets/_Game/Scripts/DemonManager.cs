﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DemonManager : MonoBehaviour
{
    #region Class Delcarations

    [Serializable]
    public class OnEntityPossessed : UnityEvent<OnEntityPossessedArgs> { }

    [Serializable]
    public class OnEntityPossessedArgs
    {
        public DemonManager Sender { get; private set; } = null;

        public BattleEntity OldEntity { get; private set; } = null;
        public BattleEntity NewEntity { get; private set; } = null;

        public OnEntityPossessedArgs(DemonManager sender, BattleEntity oldEntity, BattleEntity newEntity)
        {
            Sender = sender;
            OldEntity = oldEntity;
            NewEntity = newEntity;
        }
    }


    #endregion
    
    public static DemonManager Instance { get; private set; } = null;
    
    public float maxLife = 100.0f;
    public float life = 100.0f;

    public float damageTakenOnHit = 10.0f;

    public OnEntityPossessed onEntityPossessed = new OnEntityPossessed();
    public UnityEvent onLifeChanged = new UnityEvent();
    public UnityEvent onDamageTaken = new UnityEvent();
    public UnityEvent onDemonDeath = new UnityEvent();

    public BattleEntity CurrentPossessedEntity { get; private set; } = null;

    private void Awake() =>
        Instance = this;
    

    private async void Start()
    {
        GameBattleManager battleManager = GameBattleManager.Instance;

        await new WaitUntil(() => battleManager.leftSideEntities.Count > 0 && battleManager.CurrentBattleState == GameBattleManager.BattleState.InProgress);

        Possess(battleManager.leftSideEntities[0]);
    }

    public void TakeDamage(float damages)
    {
        if (life <= 0.0f)
            return;

        life = Mathf.Max(life - damageTakenOnHit, 0.0f);

        Debug.Log("Demon took damage");

        onLifeChanged.Invoke();
        onDamageTaken.Invoke();

        if (life <= 0.0f)
            onDemonDeath.Invoke();
    }

    public void Possess(BattleEntity entity)
    {
        BattleEntity old = CurrentPossessedEntity;

        if (old != null)
            UnregisterCallbacks(old);

        CurrentPossessedEntity = entity;

        if (CurrentPossessedEntity != null)
        {
            RegisterCallbacks(CurrentPossessedEntity);
            Debug.Log("Possessed entity " + CurrentPossessedEntity.name);
        }

        onEntityPossessed.Invoke(new OnEntityPossessedArgs(this, old, CurrentPossessedEntity));
    }

    public void ForcePossessedFocus(SelectableObject.OnClickArgs args)
    {
        if (CurrentPossessedEntity != null)
            CurrentPossessedEntity.ForceTarget(args.Sender.GetComponent<BattleEntity>());
    }

    private void RegisterCallbacks(BattleEntity entity)
    {
        entity.onDamageTaken.AddListener(OnPossessedEntityDamageTaken);
        entity.onDeath.AddListener(OnPossessedEntityDied);
    }

    private void UnregisterCallbacks(BattleEntity entity)
    {
        entity.onDamageTaken.RemoveListener(OnPossessedEntityDamageTaken);
        entity.onDeath.RemoveListener(OnPossessedEntityDied);
    }

    private void OnPossessedEntityDamageTaken(BattleEntity.OnDamageTakenArgs args)
    {
        TakeDamage(damageTakenOnHit);
    }

    private void OnPossessedEntityDied()
    {
        TakeDamage(damageTakenOnHit);

        GameBattleManager battleManager = GameBattleManager.Instance;
        if (battleManager.leftSideEntities.Count > 0)
            Possess(battleManager.leftSideEntities[0]);
        else
        {
            CurrentPossessedEntity = null;
            life = 0.0f;
            onLifeChanged.Invoke();
            onDemonDeath.Invoke();
        }
    }
}
