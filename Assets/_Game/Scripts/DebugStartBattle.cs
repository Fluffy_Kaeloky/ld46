﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugStartBattle : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Battle Started !");
        GameBattleManager.Instance.StartBattle();
    }
}
