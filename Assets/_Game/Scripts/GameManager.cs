﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; } = null;

    public UnityEvent onGameOver = new UnityEvent();
    public UnityEvent onGameEnd = new UnityEvent();

    private void Awake()
    {
        Instance = this;
    }

    public void GameOver()
    {
        Debug.LogWarning("Game over !");
        onGameOver.Invoke();
    }

    public void GameEnd()
    {
        Debug.LogWarning("Game end !");
        onGameEnd.Invoke();
    }
}
