﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class WinCanvas : MonoBehaviour
{
    private CanvasGroup canvasGroup = null;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        gameObject.SetActive(false);
        canvasGroup.alpha = 0.0f;
    }

    public void ShowPanel()
    {
        gameObject.SetActive(true);

        canvasGroup.DOFade(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);
    }

    public void OnButtonPress()
    {
        Application.Quit();
    }
}
