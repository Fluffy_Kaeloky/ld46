﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class SelectableObject : MonoBehaviour
{
    #region Class Declarations

    [Serializable]
    public class OnSwipe : UnityEvent<OnSwipeArgs> { }

    [Serializable]
    public class OnSwipeArgs
    {
        public SelectableObject Sender { get; private set; } = null;
        public Vector2 Direction { get; private set; } = Vector2.zero;

        public OnSwipeArgs(SelectableObject sender, Vector2 direction)
        {
            Sender = sender;
            Direction = direction;
        }
    }

    [Serializable]
    public class OnClick : UnityEvent<OnClickArgs> { }

    [Serializable]
    public class OnClickArgs
    {
        public SelectableObject Sender { get; private set; } = null;

        public OnClickArgs(SelectableObject sender)
        {
            Sender = sender;
        }
    }

    [Serializable]
    public class OnHover : UnityEvent<OnHoverArgs> { }

    [Serializable]
    public class OnHoverArgs
    {
        public SelectableObject Sender { get; private set; } = null;

        public OnHoverArgs(SelectableObject sender)
        {
            Sender = sender;
        }
    }

    #endregion

    public OnClick onClickDown = new OnClick();
    public OnClick onClickUp = new OnClick();
    public OnSwipe onSwipe = new OnSwipe();

    public OnHover onHoverEnter = new OnHover();
    public OnHover onHoverExit = new OnHover();

    public float minimumSwipePixelVelocity = 10.0f;

    public bool Selected { get; private set; } = false;

    private Vector2 oldMousePosition = Vector2.zero;
    private bool swipeFired = false;

    public void SelectDown()
    {
        onClickDown.Invoke(new OnClickArgs(this));
        Selected = true;
        oldMousePosition = Input.mousePosition;
    }

    public void SelectUp()
    {
        onClickUp.Invoke(new OnClickArgs(this));
        Selected = false;
    }

    public void HoverEnter() =>
        onHoverEnter.Invoke(new OnHoverArgs(this));

    public void HoverExit() =>
        onHoverExit.Invoke(new OnHoverArgs(this));

    private void Update()
    {
        if (Selected)
        {
            if (swipeFired)
                return;

            Vector2 deltaV = (Vector2)Input.mousePosition - oldMousePosition;
            float delta = deltaV.magnitude;

            if (delta / Time.deltaTime > minimumSwipePixelVelocity)
            {
                onSwipe.Invoke(new OnSwipeArgs(this, deltaV));
                swipeFired = true;
            }

            oldMousePosition = Input.mousePosition;
        }
        else if (swipeFired)
            swipeFired = false;
    }
}
