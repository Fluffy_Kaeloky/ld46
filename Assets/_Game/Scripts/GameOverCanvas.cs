﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameOverCanvas : MonoBehaviour
{
    public CanvasGroup canvasGroup = null;

    public UnityEvent onCanvasShow = new UnityEvent();

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void ShowPanel()
    {
        gameObject.SetActive(true);
        canvasGroup.DOFade(1.0f, 0.5f)
            .SetEase(Ease.OutExpo);
        onCanvasShow.Invoke();
    }

    public void OnTryAgainPressed()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnQuitPressed()
    {
        Application.Quit();
    }
}
