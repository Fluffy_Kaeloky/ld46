﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SelectableObject))]
public class DiceGenerator : MonoBehaviour
{
    #region Class Declarations

    [Serializable]
    public class OnDiceGenerated : UnityEvent<OnDiceGeneratedArgs> { }

    [Serializable]
    public class OnDiceGeneratedArgs
    {
        public DiceGenerator Sender { get; private set; } = null;

        public OnDiceGeneratedArgs(DiceGenerator sender)
        {
            Sender = sender;
        }
    }

    #endregion

    public Dice dicePrefab = null;

    public float previewRotationSpeed = 30.0f;

    public OnDiceGenerated onDiceGenerated = new OnDiceGenerated();

    public float cooldown = 5.0f;

    public bool CanGenerateDice { get; set; } = true;

    private SelectableObject selectable = null;

    private Transform preview = null;

    private void Awake()
    {
        selectable = GetComponent<SelectableObject>();
    }

    private void Start()
    {
        selectable.onSwipe.AddListener(OnSwiped);

        Dice dice = Instantiate(dicePrefab, Vector3.zero, Quaternion.identity, transform);
        dice.transform.localPosition = Vector3.zero;

        dice.Rigidbody.isKinematic = true;

        preview = dice.transform;

        Destroy(dice.GetComponent<SelectableObject>());
        Destroy(dice);

        Canvas canvas = GetComponentInParent<Canvas>();
        if (canvas != null)
        {
            preview.localScale = new Vector3(preview.localScale.x * (1.0f / canvas.transform.localScale.x),
                                             preview.localScale.y * (1.0f / canvas.transform.localScale.y),
                                             preview.localScale.z * (1.0f / canvas.transform.localScale.z));
        }
    }

    private void Update()
    {
        preview.transform.rotation *= Quaternion.Euler(previewRotationSpeed * Time.deltaTime,
                                                        previewRotationSpeed * Time.deltaTime,
                                                        previewRotationSpeed * Time.deltaTime);
    }

    private void OnSwiped(SelectableObject.OnSwipeArgs args)
    {
        if (!CanGenerateDice)
            return;

        DiceLauncher diceLauncher = DiceLauncher.Instance;

        Vector3 offset = transform.position - diceLauncher.fireMuzzle.position;
        Vector3 localOffset = transform.InverseTransformVector(offset);
        localOffset.y = 0.0f;
        localOffset.z = 0.0f;
        offset = transform.TransformVector(localOffset);

        diceLauncher.LaunchDice(dicePrefab, args.Direction.normalized, 12.0f, offset);
        onDiceGenerated.Invoke(new OnDiceGeneratedArgs(this));
    }
}
