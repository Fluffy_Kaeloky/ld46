﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDamageEffect", menuName = "LD46/Effects/DamageEffect")]
public class DamageDiceEffect : EffectBase
{
    public Target target = Target.Heroes;

    public bool applyToAll = true;
    public float damages = 5.0f;

    public override void ApplyEffect()
    {
        List<BattleEntity> targetEntities;
        if (target == Target.Enemies)
            targetEntities = new List<BattleEntity>(GameBattleManager.Instance.rightSideEntities);
        else
            targetEntities = new List<BattleEntity>(GameBattleManager.Instance.leftSideEntities);

        if (!applyToAll)
        {
            BattleEntity entity = targetEntities[Random.Range(0, targetEntities.Count)];
            targetEntities.Clear();
            targetEntities.Add(entity);
        }

        targetEntities.ForEach(x =>
        {
            if (x != null)
                x.Damage(damages, null);
        });
    }
}
