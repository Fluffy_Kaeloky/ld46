﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDebugEffect", menuName = "LD46/Effects/DebugEffect", order = 0)]
public class DebugLogEffect : EffectBase
{
    public override void ApplyEffect()
    {
        Debug.Log(effectName + " was triggered");
    }
}
