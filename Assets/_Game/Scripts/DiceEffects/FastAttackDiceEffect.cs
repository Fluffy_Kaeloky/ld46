﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFastAttackDiceEffect", menuName = "LD46/Effects/FastAttackDiceEffect")]
public class FastAttackDiceEffect : EffectBase
{
    public Target target = Target.Heroes;

    public float time = 5.0f;
    public float multiplier = 2.0f;

    public override async void ApplyEffect()
    {
        BattleController targetController;
        if (target == Target.Enemies)
            targetController = GameBattleManager.Instance.rightSideController;
        else
            targetController = GameBattleManager.Instance.leftSideController;

        targetController.chargeRateMultiplier = multiplier;

        await new WaitForSeconds(time);

        if (targetController != null)
            targetController.chargeRateMultiplier = 1.0f;
    }
}
