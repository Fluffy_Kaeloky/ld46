﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewHealEffect", menuName = "LD46/Effects/HealEffect")]
public class HealDiceEffect : EffectBase
{
    public Target target = Target.Heroes;

    public bool applyToAll = true;
    public float heal = 5.0f;

    public override void ApplyEffect()
    {
        List<BattleEntity> targetEntities;
        if (target == Target.Enemies)
            targetEntities = GameBattleManager.Instance.rightSideEntities;
        else
            targetEntities = GameBattleManager.Instance.leftSideEntities;

        if (!applyToAll)
        {
            BattleEntity entity = targetEntities[Random.Range(0, targetEntities.Count)];
            targetEntities.Clear();
            targetEntities.Add(entity);
        }

        targetEntities.ForEach(x => x.Heal(heal));
    }
}

