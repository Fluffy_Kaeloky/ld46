﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPowerAttackDiceEffect", menuName = "LD46/Effects/PowerAttackDiceEffect")]
public class PowerAttackDiceEffect : EffectBase
{
    public float addedDamageMultiplier = 2.0f;

    public override void ApplyEffect()
    {
        GameBattleManager.Instance.leftSideController.NextAttackMultiplier += addedDamageMultiplier;
    }
}
