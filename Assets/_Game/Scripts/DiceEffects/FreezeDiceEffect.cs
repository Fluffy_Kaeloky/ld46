﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFreezeEffect", menuName = "LD46/Effects/FreezeEffect")]
public class FreezeDiceEffect : EffectBase
{
    public Target target = Target.Heroes;

    public float time = 5.0f;

    public override async void ApplyEffect()
    {
        List<BattleEntity> targetEntities;
        if (target == Target.Enemies)
            targetEntities = GameBattleManager.Instance.rightSideEntities;
        else
            targetEntities = GameBattleManager.Instance.leftSideEntities;

        targetEntities.ForEach(x => x.Frozen = true);

        await new WaitForSeconds(time);

        targetEntities.ForEach(x =>
        {
            if (x != null)
                x.Frozen = false;
        });
    }
}
