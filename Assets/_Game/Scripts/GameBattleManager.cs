﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameBattleManager : SerializedMonoBehaviour
{
    public enum BattleState
    {
        InProgress,
        Resolved,
        Paused
    }

    public static GameBattleManager Instance { get; private set; } = null;

    public List<BattleEntity> leftSideEntities = new List<BattleEntity>();
    public List<BattleEntity> rightSideEntities = new List<BattleEntity>();

    public BattleController leftSideController = null;
    public BattleController rightSideController = null;

    public Transform leftSideHandle = null;
    public Transform rightSideHandle = null;

    public UnityEvent onBattleResolved = new UnityEvent();

    private BattleState currentBattleState = BattleState.Paused;
    public BattleState CurrentBattleState { get { return currentBattleState; } set
        {
            Debug.Log("Battle State changed from " + currentBattleState + " to " + value);
            currentBattleState = value;
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    public void StartBattle()
    {
        CurrentBattleState = BattleState.InProgress;
        leftSideController.InitBattle(leftSideEntities, rightSideEntities);
        rightSideController.InitBattle(rightSideEntities, leftSideEntities);
    }

    public void RemoveEntity(BattleEntity entity)
    {
        leftSideEntities.Remove(entity);
        rightSideEntities.Remove(entity);

        if (leftSideEntities.Count == 0 || rightSideEntities.Count == 0)
        {
            CurrentBattleState = BattleState.Resolved;
            Debug.Log("Battle has been resolved !");
            onBattleResolved.Invoke();
        }
    }

    public BattleController GetControllingController(BattleEntity entity)
    {
        if (rightSideEntities.Contains(entity))
            return rightSideController;
        else if (leftSideEntities.Contains(entity))
            return leftSideController;
        return null;
    }

    public Transform GetLeftSideHandle(int line, int column)
    {
        return leftSideHandle.GetChild(column).GetChild(line);
    }

    public Transform GetRightSideHandle(int line, int column)
    {
        return rightSideHandle.GetChild(column).GetChild(line);
    }

    private void Update()
    {
        if (CurrentBattleState != BattleState.InProgress)
            return;

        leftSideEntities.ForEach(x => x.UpdateEntity(leftSideController));
        rightSideEntities.ForEach(x => x.UpdateEntity(rightSideController));

        if (leftSideController != null)
            leftSideController.UpdateController(leftSideEntities, rightSideEntities);

        if (rightSideController != null)
            rightSideController.UpdateController(rightSideEntities, leftSideEntities);
    }
}
