﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

public class DiceLauncher : MonoBehaviour
{
    public static DiceLauncher Instance { get; private set; } = null;

    public Transform fireMuzzle = null;

    private void Awake()
    {
        Instance = this;
    }

    public void LaunchDice(Dice diceTemplate, Vector2 direction, float force, Vector3? positionLocalOffset = null)
    {
        Vector3 position = fireMuzzle.position;
        if (positionLocalOffset != null)
            position += positionLocalOffset.Value;

        Dice dice = Instantiate(diceTemplate, position, fireMuzzle.rotation);

        Vector3 originalScale = dice.transform.localScale;
        dice.transform.localScale = Vector3.zero;
        dice.transform.DOScale(originalScale, 0.5f);

        Vector3 localForceDir = new Vector3(direction.x, direction.y, 1.0f);

        dice.Rigidbody.AddForce(fireMuzzle.TransformDirection(localForceDir) * force, ForceMode.Impulse);
        dice.Rigidbody.angularVelocity = new Vector3(UnityEngine.Random.Range(-60.0f, 60.0f),
                                                    UnityEngine.Random.Range(-60.0f, 60.0f),
                                                    UnityEngine.Random.Range(-60.0f, 60.0f));

        dice.Launch();

        dice.onStoppedRolling.AddListener(() => 
        {
            EffectBase effect = dice.GetRolledEffect();
            Debug.Log(effect.effectName + " was rolled");

            effect.ApplyEffect();

            dice.Destroy();
        });
    }
}
