﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoreBits : MonoBehaviour
{

    public GameObject Gore;
    public Transform GorePos;
    public void GOBOOM()
    {
        Goremanager.Instance.PlayGore();
        Instantiate(Gore, GorePos.position, Quaternion.identity);
    }
}
