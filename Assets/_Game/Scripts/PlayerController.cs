﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BattleController
{
    public override void InitBattle(List<BattleEntity> entities, List<BattleEntity> enemyEntities)
    {
        if (enemyEntities.Count > 0)
            entities.ForEach(x => x.SetTarget(enemyEntities[Random.Range(0, enemyEntities.Count)]));
    }

    public override BattleEntity RequestTarget()
    {
        throw new System.NotImplementedException();
    }

    public override void UpdateController(List<BattleEntity> entities, List<BattleEntity> enemyEntities)
    {
        foreach (BattleEntity entity in entities)
        {
            if (entity.IsReady)
            {
                entity.Attack();
                if (enemyEntities.Count > 0)
                    entity.SetTarget(enemyEntities[Random.Range(0, enemyEntities.Count)]);

                entity.actionCharge = 0.0f;
            }
        }
    }
}
