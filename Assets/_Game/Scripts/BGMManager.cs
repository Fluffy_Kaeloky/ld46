﻿using AlmenaraGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MultiAudioSource))]
public class BGMManager : MonoBehaviour
{
    public AudioClip intro = null;
    public AudioClip loop = null;

    public AudioClip gameOverFanfare = null;
    public AudioClip gameWinFanfare = null;

    private MultiAudioSource audioSource = null;

    private void Awake()
    {
        audioSource = GetComponent<MultiAudioSource>();
    }

    private void Start()
    {
        audioSource.PlayOverride(intro);
        audioSource.OnLoop.AddListener(() =>
        {
            audioSource.OnLoop.RemoveAllListeners();
            audioSource.PlayOverride(loop);
        });


        GameManager.Instance.onGameEnd.AddListener(() => 
        {
            audioSource.OnLoop.RemoveAllListeners();
            audioSource.OnLoop.AddListener(() => { Destroy(audioSource); });

            audioSource.PlayOverride(gameWinFanfare);
        });

        GameManager.Instance.onGameOver.AddListener(() =>
        {
            audioSource.OnLoop.RemoveAllListeners();
            audioSource.OnLoop.AddListener(() => { Destroy(audioSource); });

            audioSource.PlayOverride(gameOverFanfare);
        });
    }
}
