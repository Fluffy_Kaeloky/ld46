﻿using Rewired;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Cursor3DSelector : MonoBehaviour
{
    //public int playerIndex = 0;
    public Camera viewCamera = null;

    public LayerMask collideLayer = new LayerMask();

    //private Player player = null;
    private SelectableObject currentSelection = null;

    private SelectableObject currentHover = null;

    private void Start()
    {
        //player = ReInput.players.GetPlayer(playerIndex);
    }

    private void Update()
    {
        Vector2 pixelCursorPosition = Input.mousePosition;
        Ray ray = viewCamera.ScreenPointToRay(pixelCursorPosition);

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, float.PositiveInfinity, collideLayer.value, QueryTriggerInteraction.Collide))
        {
            SelectableObject selectable = hitInfo.collider.gameObject.GetComponentInParent<SelectableObject>();

            if (currentHover != selectable)
            {
                if (currentHover != null)
                    currentHover.HoverExit();

                currentHover = selectable;

                if (currentHover != null)
                    currentHover.HoverEnter();
            }

            if (selectable != null)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    selectable.SelectDown();
                    currentSelection = selectable;
                }
            }
        }
        else
        {
            if (currentHover != null)
                currentHover.HoverExit();
            currentHover = null;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (currentSelection != null)
            {
                currentSelection.SelectUp();
                currentSelection = null;
            }
        }
    }
}
