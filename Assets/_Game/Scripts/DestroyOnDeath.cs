﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BattleEntity))]
public class DestroyOnDeath : MonoBehaviour
{
    public float delay = 0.0f;

    private void Awake()
    {
        GetComponent<BattleEntity>().onDeath.AddListener(() => 
        {
            Destroy(gameObject, delay);
        });
    }
}
