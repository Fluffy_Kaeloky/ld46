﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[RequireComponent(typeof(DemonManager))]
public class DemonManagerFeedback : MonoBehaviour
{
    private void Start()
    {
        DemonManager demonManager = GetComponent<DemonManager>();

        demonManager.onDamageTaken.AddListener(() => 
        {
            CameraShaker.Instances.ForEach(x => x.Shake(1.0f));
        });
    }

    public void OnEntityPossessed(DemonManager.OnEntityPossessedArgs args)
    {
        args.NewEntity.GetComponent<FeedbackSelection>().Selected();
        if(args.OldEntity != null && args.OldEntity != args.NewEntity)
        {
            args.OldEntity.GetComponent<FeedbackSelection>().UnSelected();
        }
    }
}
