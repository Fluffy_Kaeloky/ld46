﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BattleEntity))]
public class EntityAttackAnimatorManager : MonoBehaviour
{
    public Animator animator = null;

    public string attackParameterName = "Attack";

    private BattleEntity battleEntity = null;

    private void Awake()
    {
        battleEntity = GetComponent<BattleEntity>();
    }

    private void Start()
    {
        battleEntity.onEntityAttack.AddListener((args) => animator.SetTrigger(attackParameterName));
        battleEntity.onFrozenStatusChanged.AddListener(() => 
        {
            if (battleEntity.Frozen)
                animator.speed = 0.0f;
            else
                animator.speed = 1.0f;
        });
    }
}
