﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Dice : SerializedMonoBehaviour
{
    //Back, Bottom, Right, Top, Left, Front
    public Dictionary<Vector3, EffectBase> effects = new Dictionary<Vector3, EffectBase>();

    public UnityEvent onStoppedRolling = new UnityEvent();

    public UnityEvent onCollisionDetect = new UnityEvent();

    public float destroyTweenTime = 1.0f;

    private bool launched = false;
    public Rigidbody Rigidbody { get; private set; } = null;

    private List<Collider> colliders = new List<Collider>();

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>().ToList();
    }

    public EffectBase GetRolledEffect()
    {
        if (effects.Count == 0)
            return null;

        Vector3 transformedWorldUpVector = transform.InverseTransformDirection(Vector3.up);

        Debug.DrawLine(transform.position, transform.position + transformedWorldUpVector, Color.green, 2.0f);

        EffectBase rolledEffect = effects.OrderBy(x => Vector3.Angle(transformedWorldUpVector, x.Key)).First().Value;

        return rolledEffect;
    }

    public void Launch()
    {
        launched = true;
        Rigidbody.constraints = RigidbodyConstraints.None;

        colliders.ForEach(x => x.isTrigger = false);
    }

    public void Freeze()
    {
        launched = false;
        Rigidbody.constraints = RigidbodyConstraints.FreezePosition;

        colliders.ForEach(x => x.isTrigger = true);
    }

    public void UnFreeze()
    {
        Rigidbody.isKinematic = false;
        Rigidbody.constraints = RigidbodyConstraints.None;

        colliders.ForEach(x => x.isTrigger = false);
    }

    public void Destroy()
    {
        transform.DOScale(Vector3.zero, destroyTweenTime).OnComplete(() => 
        {
            Destroy(gameObject);
        });
    }

    public void Update()
    {
        if (!launched)
            return;

        Vector3 transformedWorldUpVector = transform.InverseTransformDirection(Vector3.up);

        Debug.DrawLine(transform.position, transform.position + transformedWorldUpVector, Color.yellow);

        if (Rigidbody.IsSleeping())
        {
            launched = false;
            onStoppedRolling.Invoke();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.impulse.magnitude > 2.5f)
        {
            onCollisionDetect.Invoke();
        }
    }
}
