﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner Instance { get; private set; } = null;

    private void Awake()
    {
        Instance = this;
    }

    public void SpawnEnemy(BattleEntity entityPrefab, int column, int line)
    {
        Transform handle = GameBattleManager.Instance.GetRightSideHandle(line, column);

        BattleEntity entityInstance = Instantiate(entityPrefab, handle);
        entityInstance.transform.localPosition = Vector3.zero;

        foreach(SpriteRenderer SP in entityInstance.GetComponentsInChildren<SpriteRenderer>())
        {
            if(column == 0)
            {
                if(line == 0)
                {
                    SP.sortingLayerName = "01";
                }
                else if(line == 1)
                {
                    SP.sortingLayerName = "02";
                }
                else if(line == 2)
                {
                    SP.sortingLayerName = "03";
                }
            }
            else if (column == 1)
            {
                if (line == 0)
                {
                    SP.sortingLayerName = "04";
                }
                else if (line == 1)
                {
                    SP.sortingLayerName = "05";
                }
                else if (line == 2)
                {
                    SP.sortingLayerName = "06";
                }
            }
        }

        GameBattleManager.Instance.rightSideEntities.Add(entityInstance);
    }
}
