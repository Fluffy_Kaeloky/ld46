﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityCameraFacer : MonoBehaviour
{
    [Tooltip("Will use Camera.main if null")]
    public new Camera camera = null;
    public float rotationOffset = 0.0f;

    private void Start()
    {
        if (camera == null)
            camera = Camera.main;
    }

    private void LateUpdate()
    {
        Vector3 dir = (camera.transform.position - transform.position).normalized;
        dir = Vector3.ProjectOnPlane(dir, Vector3.up).normalized;
        float angle = Vector3.SignedAngle(Vector3.forward, dir, Vector3.up);

        transform.rotation = Quaternion.AngleAxis(angle + rotationOffset, Vector3.up);
    }
}
