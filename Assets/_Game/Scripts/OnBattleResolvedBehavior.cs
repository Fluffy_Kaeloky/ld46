﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GameBattleManager))]
public class OnBattleResolvedBehavior : MonoBehaviour
{
    public DiceRewardUI diceRewardUI = null;

    private GameBattleManager battleManager = null;

    private void Awake()
    {
        battleManager = GetComponent<GameBattleManager>();
    }

    private void Start()
    {
        battleManager.onBattleResolved.AddListener(OnBattleResolved);
    }

    public void OnBattleResolved()
    {
        Debug.Log("Battle resolved behavior");

        if (battleManager.leftSideEntities.Count == 0)
            GameManager.Instance.GameOver();
        else
            StartCoroutine(_NextLevel());
    }

    private IEnumerator _NextLevel()
    {
        var diceReward = WaveManager.Instance.GetCurrentLevelDiceRewards();

        diceRewardUI.Initialize(diceReward.diceRewardLeft, diceReward.diceRewardRight);
        diceRewardUI.OpenScreen();

        yield return new WaitUntil(() => !diceRewardUI.IsOpen);

        WaveManager.Instance.PlayTransition();

        yield return new WaitForSeconds(0.5f);

        WaveManager.Instance.NextLevel();
    }
}
