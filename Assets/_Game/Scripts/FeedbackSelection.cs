﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackSelection : MonoBehaviour
{

    public SpriteRenderer spriteRenderer;
    public List<SpriteRenderer> sprites;
    public Color SelectionColor = Color.blue;
    public Color UnSelectionColor = Color.white;

    [HideInInspector]
    public bool IsSelected = false;

    private BattleEntity CurrentTarget = null;

    public GameObject TextDamageParticle;
    public Transform PosParticleDamage;
    public SpriteRenderer SelectUIHover;
    // Start is called before the first frame update
    void Start()
    {
        UnSelected();
        SelectUIHover.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        }

    public void Selected()
    {
        IsSelected = true;
        DeHoverThePlayer();
        spriteRenderer.material.SetColor("_Color",SelectionColor);
        foreach(SpriteRenderer M in sprites)
        {
            M.material.SetFloat("_Opacity", 1);
        }
    }

    public void UnSelected()
    {
        IsSelected = false;
        spriteRenderer.material.SetColor("_Color", UnSelectionColor);
        foreach (SpriteRenderer M in sprites)
        {
            M.material.SetFloat("_Opacity", 0);
        }
        if (CurrentTarget != null)
        {
            CurrentTarget.GetComponent<FeedbackEnnemy>().UnSelectedCircle();
            CurrentTarget = null;
        }
    }

    
    public void OnTargetEnnemy(BattleAdventurer.OnTargetChangedArgs args)
    {
        if (IsSelected)
        {
            args.NewTarget.GetComponent<FeedbackEnnemy>().SelectedCircle();
            CurrentTarget = args.NewTarget;
            if (args.OldTarget != null && args.OldTarget != args.NewTarget)
            {
                args.OldTarget.GetComponent<FeedbackEnnemy>().UnSelectedCircle();
            }
        }
    }

    public void ShowDamage(BattleProtoEnemy.OnDamageTakenArgs args)
    {
        GameObject TheParticle = Instantiate(TextDamageParticle, PosParticleDamage.position, Quaternion.Euler(new Vector3(0, 180, 0)));

        float damageee = Mathf.Round(args.Damages);

        if (damageee > 0)
        {
            TheParticle.GetComponent<TextMesh>().text = "-" + damageee.ToString();
        }
        else
        {
            TheParticle.GetComponent<TextMesh>().text = "+" + damageee.ToString();
            TheParticle.GetComponent<TextMesh>().color = Color.green;
        }

    }

    public void ShowHeal(BattleProtoEnemy.OnHealArgs args)
    {
        GameObject TheParticle = Instantiate(TextDamageParticle, PosParticleDamage.position, Quaternion.Euler(new Vector3(0, 180, 0)));

        float damageee = Mathf.Round(args.Amount);
        TheParticle.GetComponent<TextMesh>().text = "+" + damageee.ToString();
        TheParticle.GetComponent<TextMesh>().color = Color.green;
    }

    public void HoverThePlayer()
    {

        if (!IsSelected)
        {
            SelectUIHover.enabled = true;
        }
    }
    public void DeHoverThePlayer()
    {
        SelectUIHover.enabled = false;
    }
}
