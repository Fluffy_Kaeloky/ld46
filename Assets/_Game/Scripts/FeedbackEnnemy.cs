﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackEnnemy : MonoBehaviour
{

    public SpriteRenderer CircleSprite;
    public List<SpriteRenderer> EnnemySprites;

    public Color NoTargetColor;
    public Color TargetColor;

    private BattleEntity TargetMemory;

    public GameObject TextDamageParticle;
    public Transform PosParticleDamage;
    // Start is called before the first frame update
    void Start()
    {
        CircleSprite.enabled = false;
        DeHighlightEnnemy();
    }

    private void Update()
    {
        if(TargetMemory != null && TargetMemory.GetComponent<FeedbackSelection>().IsSelected)
        {
            HighlightEnnemy();
        }
        else if(TargetMemory != null && !TargetMemory.GetComponent<FeedbackSelection>().IsSelected)
        {
            DeHighlightEnnemy();
        }
    }

    public void SelectedCircle()
    {
        CircleSprite.enabled = true;
    }
    public void UnSelectedCircle()
    {
        CircleSprite.enabled = false;
    }

    public void HighlightEnnemy()
    {
        foreach(SpriteRenderer Sprites in EnnemySprites)
        {
            Sprites.material.SetFloat("_Opacity", 10);
            Sprites.material.SetColor("_ColorCharacter", TargetColor);
        }
    }

    public void DeHighlightEnnemy()
    {
        foreach (SpriteRenderer Sprites in EnnemySprites)
        {
            Sprites.material.SetFloat("_Opacity",0);
            Sprites.material.SetColor("_ColorCharacter", NoTargetColor);
        }
    }

    public void OnTargetHero(BattleAdventurer.OnTargetChangedArgs args)
    {
        TargetMemory = args.NewTarget;
        if (args.NewTarget.GetComponent<FeedbackSelection>().IsSelected)
        {
            HighlightEnnemy();
        }
        else if(!args.NewTarget.GetComponent<FeedbackSelection>().IsSelected && args.NewTarget != args.OldTarget)
        {
            DeHighlightEnnemy();
        }
    }

    public void ArrowTargeting(SelectableObject.OnHoverArgs args)
    {
        ArrowManager.Instance.ShowArrow(DemonManager.Instance.CurrentPossessedEntity.transform.position, this.transform.position);
    }

    public void DeArrowTargeting()
    {
        ArrowManager.Instance.HideArrow();
    }

    public void ShowDamage(BattleProtoEnemy.OnDamageTakenArgs args)
    {
        GameObject TheParticle = Instantiate(TextDamageParticle, PosParticleDamage.position, Quaternion.Euler(new Vector3(0,180,0)));
        TheParticle.GetComponent<TextMesh>().text = "-" + args.Damages.ToString();

    }



}
