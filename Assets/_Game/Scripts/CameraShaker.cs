﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraShaker : MonoBehaviour
{
    public static List<CameraShaker> Instances { get; private set; } = new List<CameraShaker>();

    public float defaultShakeAmplitude = 5.0f;

    private CinemachineBasicMultiChannelPerlin perlin = null;

    private Tweener tween = null;

    private void Awake()
    {
        Instances.Add(this);

        perlin = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private void OnDestroy()
    {
        Instances.Remove(this);
    }

    public void Shake(float time)
    {
        Shake(time, defaultShakeAmplitude);
    }

    public void Shake(float time, float amplitude)
    {
        StopShake();

        tween = DOTween.To(x => perlin.m_AmplitudeGain = x, 0.0f, amplitude, time / 2.0f)
            .SetLoops(2, LoopType.Yoyo);
    }

    public void StopShake()
    {
        if (tween != null && tween.active)
            tween.Kill();
    }
}
