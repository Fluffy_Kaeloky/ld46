﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowManager : MonoBehaviour
{
    public static ArrowManager Instance { get; private set; } = null;
    public LineRenderer lineRend;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        lineRend.enabled = false;
    }

    public void ShowArrow(Vector3 start, Vector3 end)
    {
        lineRend.enabled = true;

        lineRend.SetPosition(0, start);
        lineRend.SetPosition(1, end);
    }

    public void HideArrow()
    {
        lineRend.enabled = false;
    }


}
