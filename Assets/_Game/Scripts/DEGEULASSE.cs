﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DEGEULASSE : MonoBehaviour
{
    private Image ZIMAGE;
    // Start is called before the first frame update
    void Start()
    {
        ZIMAGE = this.GetComponent<Image>();
        ZIMAGE.color = new Color(0, 0, 0, 0);
        StartCoroutine(WaitStart());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator WaitStart()
    {
        for(; ;)
        {
            yield return new WaitForSeconds(5f);
            ZIMAGE.color = new Color(0, 0, 0, 1);
            StopAllCoroutines();
        }
    }
}
