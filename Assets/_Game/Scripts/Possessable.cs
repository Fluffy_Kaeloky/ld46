﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BattleEntity))]
public class Possessable : MonoBehaviour
{
    private BattleEntity battleEntity = null;

    private void Awake()
    {
        battleEntity = GetComponent<BattleEntity>();
    }

    public void TakePossession()
    {
        DemonManager.Instance.Possess(battleEntity);
    }
}
