﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLevelDescriptor", menuName = "LD46/LevelDescriptor")]
public class LevelDescriptor : SerializedScriptableObject
{
    public int environementIndex = 0;

    public List<EnemySpawnDescriptor> enemyDescriptors = new List<EnemySpawnDescriptor>();

    public DiceRewardDescriptor diceRewards = new DiceRewardDescriptor();
}

[Serializable]
public struct EnemySpawnDescriptor
{
    public int column;
    public int line;
    public BattleEntity entity;
}

[Serializable]
public struct DiceRewardDescriptor
{
    public DiceGenerator diceRewardLeft;
    public DiceGenerator diceRewardRight;
}
